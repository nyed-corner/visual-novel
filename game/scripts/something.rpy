
label nicu:

    e "Oh, hello to my new file"
    define drank_knifes = False

menu:
    "What should I do?"

    "Drink coffee.":
        "I drink the coffee."
        jump drank_coffee
    "Drink tea.":
        "I drink the tea."
    "Knife":
        "This was a stupid idea"
        $ drank_knifes = True
        jump next2

label next:
    "Done drinking"
    jump next2

label next2:

menu:
    "So, where do you want to go?"

    "McDonalds":
        return
    "Burger King" if not drank_knifes:
        return
    "Hospital" if drank_knifes:
        return

label drank_coffee:
    e "I did not think you would drink that -dies-"
    return