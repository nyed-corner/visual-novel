label relations:
    define relEileen = 0

menu:
    "What do you want to drink?"

    "Coffee":
        $ relEileen = change_relation(relEileen, 2)
    "Tea":
        $ relEileen += change_relation(relEileen, 0)
    "Hot Chocolate":
        $ relEileen -= change_relation(relEileen, -2)

label next3:
    e "Wow, our relationship is at [relEileen] now"
    return
