init python:
    def change_relation(relStart, relDiff):
        result = relStart + relDiff
        if result < -4:
            result = -4
        elif result > 12:
            result = 12
        return result

